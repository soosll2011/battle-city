using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotator : MonoBehaviour
{
    private Vector3 _currentRotation;
    public Vector3 CurrentRotation => _currentRotation;

    public void SetRotation(Vector3 rotation)
    {
        _currentRotation = rotation;
    }
}
