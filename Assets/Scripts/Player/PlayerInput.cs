using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private float _inputHorizontal;
    private float _inputVertical;

    private Vector3 _moveDirection;
    public Vector3 MoveDirection => _moveDirection;

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
            _moveDirection = Vector3.up;
        else if (Input.GetKey(KeyCode.S))
            _moveDirection = Vector3.down;
        else if (Input.GetKey(KeyCode.A))
            _moveDirection = Vector3.left;
        else if (Input.GetKey(KeyCode.D))
            _moveDirection = Vector3.right;

        else
            _moveDirection = Vector3.zero;
    }
}
