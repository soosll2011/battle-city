using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour
{
    [SerializeField] private float _moveSpeed;

    private Rigidbody2D _rigidbody;
    private PlayerInput _playerInput;

    private void Awake()
    {
        _playerInput = GetComponent<PlayerInput>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Move()
    {
        _rigidbody.velocity = _playerInput.MoveDirection * _moveSpeed;
    }

    private void FixedUpdate()
    {
        Move();
    }
}
